import java.util.Collections;
import java.util.Objects;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        name = n;
        firstChild = d;
        nextSibling = r;
    }

    @Override
    public String toString() {
        String endString = "";


        if (firstChild == null && nextSibling == null) {
            endString += name;
        } else if (firstChild == null) {
            endString += name + "," + nextSibling;
        } else if (nextSibling == null) {
            endString += name + "(" + firstChild + ")";
        } else {
            endString += name + "(" + firstChild + ")" + "," + nextSibling;
        }

        return endString;
    }

    public static Node parsePostfix(String s) { //teeb valmis p

        checkIfInputStringIsCorrect(s);

        if (s.length() == findTheLengthOfTheElement(s, 0) && !s.contains(" ")) {
            return new Node(s, null, null);
        }
        checkIfInputStringHasCorrectStructure(s);


        Node root = new Node(s.substring(s.length() - 1), checkNextNode(s.substring(1, s.length() - 2)), null);

        return root;
    }


    public static Node checkNextNode(String s) {
//      (((G,H)D,E)B,(J)C)A

        if (checkIfLastElement(s)) {
            return new Node(s, null, null);
        }

        char[] elements = s.toCharArray();

        int parenthesisCounter1 = 0;
        int parenthesisCounter2 = 0;

        for (int i = 0; i < elements.length; i++) {

            if (elements[i] == '(') {
                parenthesisCounter1++;
            } else if (elements[i] == ')') {
                parenthesisCounter2++;
            }

            if (parenthesisCounter1 == parenthesisCounter2 && elements[i] != '(' && elements[i] != ')') {

                int lengthOfElement = Node.findTheLengthOfTheElement(s, i) - 1; // et ma teaks, kui pikk on praegune elemet

                if (i + lengthOfElement == elements.length - 1) {
                    return new Node(s.substring(i, i + lengthOfElement + 1), checkNextNode(s.substring(1, i - 1)), null);
                } else if (elements[i + lengthOfElement + 1] == ',' && i == 0) {
                    return new Node(s.substring(i, i + lengthOfElement + 1), null, checkNextNode(s.substring(i + lengthOfElement + 2)));
                } else if (elements[i + lengthOfElement + 1] == ',') {
                    return new Node(s.substring(i, i + lengthOfElement + 1), checkNextNode(s.substring(1, i - 1)), checkNextNode(s.substring(i + lengthOfElement + 2)));
                }
            }
        }
        return null;
    }

    public static int findTheLengthOfTheElement(String s, int currentIndex) {
        char[] elements = s.toCharArray();
        int lengthOfElement = 0;

        for (int i = currentIndex; i < elements.length; i++) {
            if (elements[i] == ')') {
                break;
            } else if (elements[i] == ',') {
                break;
            }
            lengthOfElement++;
        }
        return lengthOfElement;
    }


    public static boolean checkIfLastElement(String s) {
        return !s.contains(",") && !s.contains("(") && !s.contains(")");
    }


    public static void checkIfInputStringIsCorrect(String s) {

        if (s.contains(",,") || s.contains("))") || s.contains("(,") || s.contains(",)")) {
            throw new RuntimeException("String contains illegal syntax: {,,}  {(,}  {,)}  or  {))} :" + s);
        }
        if (s.contains("()")) {
            throw new RuntimeException("String contains EmptySubtree {()} :" + s);
        }
        if (s.contains("\t")) {
            throw new RuntimeException("String contains tab {\t} :" + s);
        }
    }


    public static void checkIfInputStringHasCorrectStructure(String s) {
        if (!s.substring(0, 1).equals("(")) {
            throw new RuntimeException(" Juurtipu alamtippe peavad ümbritsema sulud :" + s);
        }
        char[] elements = s.toCharArray();

        int parenthesisCounter1 = 0;
        int parenthesisCounter2 = 0;
        int indexOfLastparenthesis = 0;

        for (int i = 0; i < elements.length; i++) {

            if (elements[i] == '(') {
                parenthesisCounter1++;
            } else if (elements[i] == ')') {
                parenthesisCounter2++;
                indexOfLastparenthesis = i;
            }
            if (parenthesisCounter2 > parenthesisCounter1) {
                throw new RuntimeException("Sulg: {)} on vales kohas:" + s); // (( hhh ))) iii (() - see pole lubatud
            }
           if (elements[i] == ' ') {
              if (elements[i-1] != ','){
                 throw new RuntimeException("Whitespace is only  allowed after comma:" + s);
              }
           }
        }
        if (parenthesisCounter1 != parenthesisCounter2) {
            throw new RuntimeException("Sulgude arv sõnes pole võrdne:" + s);
        }

        String str = s.substring(indexOfLastparenthesis);
        if (str.contains(",") || str.contains("(")) {
            throw new RuntimeException("Juurtipus on keelatud elemendid:" + s);
        }
        String sr = s.substring(1, indexOfLastparenthesis - 1);
        if (sr.indexOf("(") > sr.indexOf(")")) {
            throw new RuntimeException("Juurtipul puuduvad õiged sulud:" + s);
        }

    }
    public String leftParentheticRepresentation() {
        StringBuffer endString = new StringBuffer();

        if (firstChild == null && nextSibling == null) {
            endString.append(name);
        } else if (firstChild == null) {
            endString.append(name).append(",").append(nextSibling);
        } else if (nextSibling == null) {
            endString.append(name).append("(").append(firstChild).append(")");
        } else {
            endString.append(name).append("(").append(firstChild).append(")").append(",").append(nextSibling);
        }

        return endString.toString();
    }

    public String pseudoXml(int startingDebt){

        if (startingDebt < 1){
            throw new RuntimeException("StartingDebt must be higer than or equal to 1| given startingDebt:" + startingDebt);
        }

        String psudoString = "";
        String distanceFromStart = "    ";

        if (firstChild == null && nextSibling == null) {
            psudoString = psudoString + String.format("%s<L%s> %s </L%s>", distanceFromStart.repeat(startingDebt - 1),startingDebt, name, startingDebt);
        } else if (firstChild == null) {
            psudoString = psudoString + String.format("%s<L%s> %s </L%s>",  distanceFromStart.repeat(startingDebt - 1), startingDebt, name, startingDebt)
                    + "\n" + nextSibling.pseudoXml(startingDebt);
        }else if (nextSibling == null) {
            psudoString = psudoString + String.format("%s<L%s> %s\n", distanceFromStart.repeat(startingDebt - 1), startingDebt, name)
                    + firstChild.pseudoXml(startingDebt + 1) + "\n" + String.format("%s</L%s>",distanceFromStart.repeat(startingDebt - 1), startingDebt);
        }else {
            psudoString = psudoString + String.format("%s<L%s> %s\n", distanceFromStart.repeat(startingDebt - 1),startingDebt, name) +
                    firstChild.pseudoXml(startingDebt + 1) + "\n" + String.format("%s</L%s>",distanceFromStart.repeat(startingDebt - 1), startingDebt)
                    + "\n" + nextSibling.pseudoXml(startingDebt);
        }

        return psudoString;

    }

    public static void main(String[] param) {
        String s = "((C,K,(L,O)P)B,(E,F)D,G)A"; //; "(B1,C)A" "((((E)D)C)B)A"
        Node t = Node.parsePostfix(s);
        //String v = t.leftParentheticRepresentation();
        String v = t.pseudoXml(1);
        System.out.println(s + " ==> " + "\n" + v); // (B1,C)A ==> A(B1,C)
    }
}


